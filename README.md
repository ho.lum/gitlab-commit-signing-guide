# GitLab commit signing with Yubikey

This is a step-by-step guide on how to create/manage/operate the commit signing with Yubikey.

Reference: [Signing git commits with Yubikey](https://bctechnology.atlassian.net/wiki/spaces/SHARING/blog/2018/05/16/369263349/Signing+git+commits+with+Yubikey)

# Table of contents

This guide consists of two parts, the admin guide and user guide.

1. [Admin Guide](admin-guide.md)
2. [User Guide](user-guide.md)