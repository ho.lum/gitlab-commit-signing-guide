# [Admin Guide] GitLab commit signing with Yubikey 

This guide is for administrator to create/manage GPG keys and commit signature with Yubikey

For Mac users, please install the following packages using [Homebrew](https://brew.sh/).
```bash
brew install gnupg gnupg2 pinentry-mac
```
For Ubuntu users, please intall the following packages
```
sudo apt-get install -y gnupg2 gnupg-agent pinentry-curses scdaemon pcscd yubikey-personalization libusb-1.0-0-dev
```
## Table of contents

- [Create user's master key](#create-users-master-key)
- [Create user's signing sub key](#create-users-signing-sub-key)
- [Add user's subkey to Yubikey](#add-users-subkey-to-yubikey)
- [Export user's public key](#export-users-public-key)
- [Distribute user's public key](#distribute-users-public-key)
- [Add user's public key to GitLab account](#add-users-public-key-to-gitlab-account)
- [Revoke user's sub key](#revoke-users-sub-key)
- [Optional: Backup and remove secret key](#optional-backup-and-remove-secret-key)

## Create user's master key 
```bash
gpg --full-gen-key
```
You will then entered the interactive shell.

Enter `4` to select RSA sign only key.

```bash
gpg (GnuPG) 2.3.2; Copyright (C) 2021 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Please select what kind of key you want:
   (1) RSA and RSA
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
   (9) ECC (sign and encrypt) *default*
  (10) ECC (sign only)
  (14) Existing key from card
Your selection? 4
```

Enter `4096` to select the key size.

```bash
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072)4096
```

Enter `0` to select the key expired time.

```bash
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0)
```

Confirm your settings, enter your name, email and comment then enter letter `O` to continue.

```bash
Is this correct? (y/N) y

GnuPG needs to construct a user ID to identify your key.

Real name: ho.lum
Email address: ho.lum@osl.com
Comment: Demo master key for gpg commit signing
You selected this USER-ID:
    "ho.lum (Demo master key for gpg commit signing) <ho.lum@osl.com>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
```

Enter the passphrase of the master key.
**Please use a strong passphrase and keep it secretly**

Re-enter the passphrase.

The master key should be ready now. Verify it.
```bash
gpg --list-key
```
```bash
/Users/holum/.gnupg/pubring.kbx
-------------------------------
pub   rsa4096 2021-10-11 [SC]
      616F615A97B1FFC440F0A25FCC718E0E03AB1FFD
uid           [ultimate] ho.lum (master key for user ho.lum) <ho.lum@osl.com>
sub   rsa4096 2021-10-11 [S]
```

## Create user's signing sub key

Enter the following command to start adding a user's signing subkey
```bash
gpg --edit-key <NAME | EMAIL | ID>
# for example
# gpg --edit-key ho.lum or
# gpg --edit-key ho.lum@osl.com or
# gpg --edit-key 616F615A97B1FFC440F0A25FCC718E0E03AB1FFD
```
You will then entered the interactive shell.

Enter `addkey` to add sub key

```bash
gpg> addkey
```

Enter `4` to select RSA sign only key.

```
Please select what kind of key you want:
   (3) DSA (sign only)
   (4) RSA (sign only)
   (5) Elgamal (encrypt only)
   (6) RSA (encrypt only)
  (10) ECC (sign only)
  (12) ECC (encrypt only)
  (14) Existing key from card
Your selection? 4
```

Enter `4096` to select the key size.

```bash
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072)4096
```

Enter `0` to select the key expired time.

```bash
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0)
```

Confirm settings and enter `y` twice
```bash
Is this correct? (y/N) y
Really create? (y/N) y
```

Enter the passphrase of the master key we have created.

You should see a new subkey added
```bash
ssb  rsa4096/25BE884506A6BB42
     created: 2021-10-11  expires: never       usage: S
```
Enter `save` before exit the interactive shell 

```bash
gpg> save
```

## Add user's subkey to Yubikey

First, insert the Yubikey and verify your Yubikey is readable by GPG

```bash
gpg --card-status
```

The output should be similar to below:

```bash
Reader ...........: Yubico YubiKey OTP FIDO CCID
Application ID ...: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Application type .: OpenPGP
Version ..........: 0.0
Manufacturer .....: Yubico
Serial number ....: 0000000000
.....
```

If GPG cannot read Yubikey. Please check your system setting.

Then enter the following command to start add a user's signing subkey to Yubikey

```bash
gpg --edit-key <NAME | EMAIL | ID>
# for example
# gpg --edit-key ho.lum or
# gpg --edit-key ho.lum@osl.com or
# gpg --edit-key F051FBEE81F0FB2755C4B0F14C0858590D41E2EF
```

A list of available keys should be shown 

```bash
Secret subkeys are available.

pub  rsa4096/CC718E0E03AB1FFD
     created: 2021-10-11  expires: never       usage: SC
     trust: ultimate      validity: ultimate
ssb  rsa4096/04F195E73CE2FBA3
     created: 2021-10-11  expires: never       usage: S
     card-no: 0006 15783215
```
Select the signing subkey that has just been created

```bash
gpg> key 1 
# the number depends on the order of list shown
# For the above example, our signing key is the first subkey,
# so the order will be 1
```

You should see the select the key is marked with a * 

```bash
ssb* rsa4096/04F195E73CE2FBA3
```

Enter `keytocard` to add subkey 

**Important: This operation will remove the secret key of the subkey from the keyring and add it to the Yubikey.**

```bash
gpg> keytocard
```
Select (1) Signature key

```bash
Please select where to store the key:
   (1) Signature key
   (3) Authentication key
Your selection? 1
```

If a key has already been stored on Yubikey, please check carefully is it OK to proceed.

Enter the passphrase of the master key if prompted.

Enter the Admin pin of the card if prompted, the default pin is `12345678`

Remember to save the settings

```bash
gpg> save
```

## Export user's public key

First get the key id of user
```bash
gpg --list-key <NAME | EMAIL | ID>
# for example
# gpg --list-key ho.lum or
# gpg --list-key ho.lum@osl.com or
# gpg --list-key 9F891C750E492ECF6506EB26ACDFCA5295CA8A00
```
The following output will printed, copy the pub id 
```bash
pub   rsa4096 2021-10-11 [SC]
      9F891C750E492ECF6506EB26ACDFCA5295CA8A00 # THIS IS THE ID
uid           [ultimate] ho.lum (master key of user ho.lum) <ho.lum@osl.com>
sub   rsa4096 2021-10-11 [S]
```
Export the public key to file
```bash
gpg --export --armor --output <EMAIL>.gpg <KEY ID>
# for example
gpg --export --armor --output ho.lum@osl.com.gpg 9F891C750E492ECF6506EB26ACDFCA5295CA8A00
```

## Distribute user's public key

In previous section, you should saved a user's public key file `<user-email>.gpg`.

You can copy the content of public file and distribute to the user via LastPass or other method.

## Add user's public key to GitLab account

Current Gitlab Saas cannot update user's gpg key in web ui.

```
GET /users?username=jack_smith
```

## Revoke user's sub key

Delete key: Removing this GPG key does not affect already signed commits.

Revoke: 

## Optional: Backup and remove secret key

