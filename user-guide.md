# [User Guide] GitLab commit signing with Yubikey 

This guide is for user sign commit with Yubikey.

For Mac users, please install the following packages using [Homebrew](https://brew.sh/).
```bash
brew install gnupg gnupg2 pinentry-mac
```
For Ubuntu users, please intall the following packages
```bash
sudo apt-get install -y gnupg2 gnupg-agent pinentry-curses scdaemon pcscd yubikey-personalization libusb-1.0-0-dev
```
## Table of contents
- [Receive your public key and Yubikey](#receive-your-public-key-and-yubikey)
- [Import your public key to GPG](#import-your-public-key-to-gpg)
- [Import your public key to Gitlab](#import-your-public-key-to-gitlab)
- [Config git](#config-git)
- [Sign commit and push code](#sign-commit-and-push-code)
- [Verify signature](#verify-signature)

## Prepare your public key and Yubikey

You should receive two items from administrator.

1. The public key file (share via LastPass or other method)
2. The Yubikey
    
## Import your public key to GPG

Enter the following command
```bash
gpg --import <PUBLIC KEY FILE NAME>
# for example 
# gpg --import ho.lum@osl.com.gpg
```

## Import your public key to Gitlab

Copy the public key file content to clipboard
```bash
# For Mac
pbcopy <  <PUBLIC KEY FILE NAME>
# for example
# pbcopy < ho.lum@osl.com.gpg
```

Open the browser, go to `https://gitlab.com/-/profile/gpg_keys`

Login to your account and paste the content to `Key` section
![Add gpg key to gitlab](img/gitlab-gpg-add.png "Add gpg key to gitlab")

Click `Add key`

You should see your gpg key added and verified (Verified here means the gpg key email match with your gitlab acount email)
![View gpg key in gitlab](img/gitlab-gpg-view.png "View gpg key in gitlab")

## Config git

First retrieve your sub key id 
```bash
gpg --list-secret-keys --keyid-format LONG
```
```
ssb>  rsa4096/<YOUR_KEY> 2021-10-11 [S]
```
```
git config --global user.signingkey <YOUR_KEY>
```
```
git config --global commit.gpgsign true
```
```
git config --global gpg.program /opt/homebrew/bin/gpg
```

## Sign commit and push code

You can now commit and push your code as normal
```
git commit -m "<commit message>"
```

If a PIN request prompted, default PIN is `123456`

```
git push # push your code !
```

## Troubleshoot 

Fail to sign the data
```
error: gpg failed to sign the data

fatal: failed to write commit object
```
```
# solution
killall gpg-agent
```

Inappropriate ioctl for device
```
gpg: signing failed: Inappropriate ioctl for device
```
```
# solution
export GPG_TTY=$(tty)  
```